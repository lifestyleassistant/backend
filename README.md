## Деньки | backend

Проект представляет собой расширенный SmartApp Framework от SberDevices;
добавлены сервисы авторизации через OAuth2, а так же поддержка умных девайсов.

* * *

-  Запустить docker-compose:
 
 `docker-compose build`
 `docker-compose up`


