#!/usr/bin/env python
import os
import sys

os.environ.setdefault("SMART_KIT_APP_CONFIG", "app_config")
os.environ.setdefault("API_URL", f"http://{os.environ['HANDLER_URL']}:{os.environ['HANDLER_PORT']}/api/")

from smart_kit.management.app_manager import execute_from_command_line

execute_from_command_line(sys.argv)
