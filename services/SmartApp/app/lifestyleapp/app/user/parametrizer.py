from scenarios.user.parametrizer import Parametrizer
import requests
import json
import os


def get_user(user_id):
    resp = requests.post(os.environ["API_URL"] + "users/get/", data={"user_id": user_id})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")
        return None
    resp = resp.json()
    return resp["user"][0]["fields"]


def get_tasks(user_id):
    resp = requests.post(os.environ["API_URL"] + "tasks/all/", data={"user_id": user_id})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")
        return None
    resp = resp.json()
    return resp["tasks"]


class CustomParametrizer(Parametrizer):

    """
        Тут можно добавить новые данные, которые будут доступны для использования в ASL при использовании jinja
    """

    def prepare_data(self, data):
        user_id = data["uuid"]["userId"]
        user = get_user(user_id)
        tasks = get_tasks(user_id)

        if "global" not in data:
            data["global"] = {}
        data["global"]["name_not_filled"] = user["name"] == "NaN"
        data["global"]["age_not_filled"] = user["age"] == -1
        data["global"]["user_name"] = user["name"]
        data["global"]["user_age"] = user["age"]
        data["global"]["user_id"] = user["user_id"]
        data["global"]["all_tasks"] = json.dumps(tasks)

        tasks_pronounce = ""
        for task in tasks:
            tasks_pronounce += task["fields"]["name"] + ". "

        data["global"]["tasks_pronounce"] = tasks_pronounce

        return data


    def __init__(self, user, items):
        super(CustomParametrizer, self).__init__(user, items)

    def _get_user_data(self, text_preprocessing_result=None):
        data = super(CustomParametrizer, self)._get_user_data(text_preprocessing_result)
        data.update({})

        data = self.prepare_data(data)

        print("DAAAATAAAA: ", data)

        return data
