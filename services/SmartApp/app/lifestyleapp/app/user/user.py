from lazy import lazy

from scenarios.user.user_model import User

from .parametrizer import CustomParametrizer
import requests
import os


def create_user(user_id):
    resp = requests.post(os.environ["API_URL"] + "users/register/", data={"user_id": user_id})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")


class CustomeUser(User):
    """
        Класс User - модель для хранения данных конкретного пользователя
        в метод fields можно добавляются собственные поля, для использования внутри базовых сущностей
    """

    def __init__(self, id, message, db_data, settings, descriptions, parametrizer_cls, load_error=False):
        create_user(id)
        super(CustomeUser, self).__init__(id, message, db_data, settings,
                                          descriptions, parametrizer_cls, load_error)

    @property
    def fields(self):
        return super(CustomeUser, self).fields + []

    @lazy
    def parametrizer(self):
        return CustomParametrizer(self, {})

    def expire(self):
        super(CustomeUser, self).expire()
