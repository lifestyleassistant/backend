from smart_kit.models.smartapp_model import SmartAppModel
import requests
import os


def set_user_name(user_id, name):
    resp = requests.post(os.environ["API_URL"] + "users/setName/", data={"user_id": user_id, "user_name": name})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")


def set_user_age(user_id, age):
    resp = requests.post(os.environ["API_URL"] + "users/setAge/", data={"user_id": user_id, "age": age})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")


def create_task(user_id, name):
    resp = requests.post(os.environ["API_URL"] + "tasks/create/", data={"user_id": user_id, "task_name": name})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")


def delete_task(user_id, task_id):
    resp = requests.post(os.environ["API_URL"] + "tasks/done/", data={"user_id": user_id, "task_order": task_id})
    if not resp.ok:
        print(f"ERROR: {resp.status_code} | {resp.text}")    


def update_command_handler(user_id, intent, message):
    if intent == "fill_name_scenario":
        set_user_name(user_id, message)
    elif intent == "fill_age_scenario":
        set_user_age(user_id, message)
    elif intent == "new_task_scenario":
        create_task(user_id, message)
    elif intent == "delete_task":
        print("DELETE TASK CALLED!!!!")
        delete_task(user_id, message)



def update_action_handler(user_id, action):
    print("UPDATE ACTION HANDLER: ", action)


class CustomModel(SmartAppModel):
    """
        В собственной Model можно добавить новые хендлеры для обработки входящих сообщений
    """

    def answer(self, message, user):
        if "intent" not in message.payload:
            update_action_handler(user.id, message.payload["action"])
        else:
            update_command_handler(user.id, message.payload["intent"],
                                   message.payload["message"]["original_text"])
        return super(CustomModel, self).answer(message, user)

    def __init__(self, resources, dialogue_manager_cls, custom_settings, **kwargs):
        super(CustomModel, self).__init__(resources, dialogue_manager_cls, custom_settings, **kwargs)
        self._handlers.update({})
