import os
import flask
import requests
from flask import request
import json
import time

import google.oauth2.credentials
import google_auth_oauthlib.flow
import googleapiclient.discovery
import os

# This variable specifies the name of a file that contains the OAuth 2.0
# information for this application, including its client_id and client_secret.
CLIENT_SECRETS_FILE = "client_id.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['openid', 'https://www.googleapis.com/auth/fitness.sleep.read']
API_SERVICE_NAME = 'fitness'
API_VERSION = 'v1'

app = flask.Flask(__name__)
# Note: A secret key is included in the sample so that it works.
# If you use this code in your application, replace this with a truly secret
# key. See https://flask.palletsprojects.com/quickstart/#sessions.
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

HANDLER_API_URL = f"http://{os.environ['HANDLER_URL']}:{os.environ['HANDLER_PORT']}/api/"


@app.route('/')
def index():
    return print_index_table()


@app.route('/parse')
def test_api_request():
    if "user_id" in request.args:
        flask.session["user_id"] = request.args.get("user_id")

    # startTimeSecs = 1614452557
    # endTimeSecs = 1614352557

    if 'credentials' not in flask.session:
        return flask.redirect('authorize')

    # Load credentials from the session.
    # credentials = google.oauth2.credentials.Credentials(
    #     **flask.session['credentials'])
    #
    # fitness = googleapiclient.discovery.build(
    #     API_SERVICE_NAME, API_VERSION, credentials=credentials)
    #
    # dataSource = fitness.users().dataSources().list(userId='me').execute()
    # if len(dataSource['dataSource']) == 0:
    #     return flask.jsonify({"ok": False, "error": "No devices"})
    #
    # dataSourceId = dataSource['dataSource'][0]['dataStreamId']
    # dataType = dataSource['dataSource'][0]['dataType']['name']
    # print("DEBUG: ", dataSourceId, dataType)
    # body = {
    #     "aggregateBy": [
    #         {
    #             "dataSourceId": "derived:com.google.sleep.segment:com.google.android.gms:merged",
    #             "dataTypeName": "com.google.sleep.segment"
    #         }
    #     ],
    #     "endTimeMillis": startTimeSecs * 1000,
    #     "startTimeMillis": endTimeSecs * 1000
    # }
    # files = fitness.users().dataset().aggregate(userId='me', body=body).execute()
    #
    # for i in range(len(files['bucket'][0]['dataset'][0]['point'])):
    #     a = files['bucket'][0]['dataset'][0]['point'][i]
    #     files['bucket'][0]['dataset'][0]['point'][i] = [str(int(int(a['startTimeNanos']) / 1000000000)),
    #                                                     str(int(int(a['endTimeNanos']) / 1000000000)),
    #                                                     'awake' if a['value'][0]['intVal'] == 1 else 'sleep']
    #
    # # Save credentials back to session in case access token was refreshed.
    # # ACTION ITEM: In a production app, you likely want to save these
    # #              credentials in a persistent database instead.
    #
    # files = files['bucket'][0]['dataset'][0]['point']
    # flask.session['credentials'] = credentials_to_dict(credentials)

    return flask.jsonify({"OK": True, "kek": "Тут будет страничка с успешной авторизацией"})


@app.route('/authorize')
def authorize():
    # Create flow instance to manage the OAuth 2.0 Authorization Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES)

    # The URI created here must exactly match one of the authorized redirect URIs
    # for the OAuth 2.0 client, which you configured in the API Console. If this
    # value doesn't match an authorized URI, you will get a 'redirect_uri_mismatch'
    # error.
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True, _scheme="https")

    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        access_type='offline',
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    # Store the state so the callback can verify the auth server response.
    flask.session['state'] = state

    return flask.redirect(authorization_url)


@app.route('/oauth2callback')
def oauth2callback():
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = flask.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        CLIENT_SECRETS_FILE, scopes=SCOPES, state=state)
    flow.redirect_uri = flask.url_for('oauth2callback', _external=True, _scheme="https")

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = flask.request.url
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    flask.session['credentials'] = credentials_to_dict(credentials)

    resp = requests.post(f"{HANDLER_API_URL}users/setGoogleFitCode/", data={"user_id": flask.session["user_id"],
                                                                            "credentials": json.dumps(flask.session[
                                                                                "credentials"])})
    if not resp.ok:
        print("RESPONSE TO HANDLER ERROR: ", resp.text)

    return flask.redirect(flask.url_for('test_api_request'))


@app.route('/revoke')
def revoke():
    if 'credentials' not in flask.session:
        return ('You need to <a href="/authorize">authorize</a> before ' +
                'testing the code to revoke credentials.')

    credentials = google.oauth2.credentials.Credentials(
        **flask.session['credentials'])

    revoke = requests.post('https://oauth2.googleapis.com/revoke',
                           params={'token': credentials.token},
                           headers={'content-type': 'application/x-www-form-urlencoded'})

    status_code = getattr(revoke, 'status_code')
    if status_code == 200:
        return ('Credentials successfully revoked.' + print_index_table())
    else:
        return ('An error occurred.' + print_index_table())


@app.route('/clear')
def clear_credentials():
    if 'credentials' in flask.session:
        del flask.session['credentials']
    return ('Credentials have been cleared.<br><br>' +
            print_index_table())


def credentials_to_dict(credentials):
    return {'token': credentials.token,
            'refresh_token': credentials.refresh_token,
            'token_uri': credentials.token_uri,
            'client_id': credentials.client_id,
            'client_secret': credentials.client_secret,
            'scopes': credentials.scopes}


def print_index_table():
    return ('<table>' +
            '<tr><td><a href="/parse">Test an API request</a></td>' +
            '<td>Submit an API request and see a formatted JSON response. ' +
            '    Go through the authorization flow if there are no stored ' +
            '    credentials for the user.</td></tr>' +
            '<tr><td><a href="/authorize">Test the auth flow directly</a></td>' +
            '<td>Go directly to the authorization flow. If there are stored ' +
            '    credentials, you still might not be prompted to reauthorize ' +
            '    the application.</td></tr>' +
            '<tr><td><a href="/revoke">Revoke current credentials</a></td>' +
            '<td>Revoke the access token associated with the current user ' +
            '    session. After revoking credentials, if you go to the test ' +
            '    page, you should see an <code>invalid_grant</code> error.' +
            '</td></tr>' +
            '<tr><td><a href="/clear">Clear Flask session credentials</a></td>' +
            '<td>Clear the access token currently stored in the user session. ' +
            '    After clearing the token, if you <a href="/test">test the ' +
            '    API request</a> again, you should go back to the auth flow.' +
            '</td></tr></table>')


if __name__ == '__main__':
    # When running locally, disable OAuthlib's HTTPs verification.
    # ACTION ITEM for developers:
    #     When running in production *do not* leave this option enabled.
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    # Specify a hostname and port that are set as a valid redirect URI
    # for your API project in the Google API Console.
    app.run('0.0.0.0', 80, debug=True)
