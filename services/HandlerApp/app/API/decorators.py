from django.http.response import JsonResponse
from .models import User


def invalid_request_method():
    return JsonResponse({
        'ok': False,
        'error_code': 404,
    })


# Checks that the request method is POST
def is_post(view):
    def check_request(request):
        if request.method != 'POST':
            return invalid_request_method()
        return view(request)

    return check_request


# Checks that the request method is GET
def is_get(view):
    def check_request(request):
        if request.method != 'GET':
            return invalid_request_method()
        return view(request)

    return check_request

# Inserts fetched user information
def personal_request(view):
    def set_variables(request):
        user_id = request.POST["user_id"]
        user = User.objects.get(user_id=user_id)
        return view(request, user)

    return set_variables
