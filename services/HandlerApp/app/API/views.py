from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import User, Task
from django.core import serializers
from .decorators import is_post, personal_request, is_get
import json


@csrf_exempt
@is_post
def register_user(request):
    user_id = request.POST["user_id"]
    if User.objects.filter(user_id=user_id).count() > 0:
        return JsonResponse({"ok": True, "info": "User already created"})
    user = User(user_id=user_id)
    user.save()
    return JsonResponse({"ok": True})


@csrf_exempt
@is_post
@personal_request
def get_user(request, user):
    user_j = serializers.serialize('json', [user, ])
    return JsonResponse({"ok": True, "user": json.loads(user_j)})


@csrf_exempt
@is_post
@personal_request
def set_user_name(request, user):
    user_name = request.POST["user_name"]
    user.name = user_name
    user.save()
    return JsonResponse({"ok": True})


@csrf_exempt
@is_post
@personal_request
def set_user_age(request, user):
    user.age = int(request.POST["age"])
    user.save()
    return JsonResponse({"ok": True})


@csrf_exempt
@is_post
@personal_request
def create_task(request, user):
    name = request.POST["task_name"]
    task = Task(user=user, name=name)
    task.save()
    return JsonResponse({"ok": True})


@csrf_exempt
@is_post
@personal_request
def set_google_fit_code(request, user):
    user.google_fit_credentials = request.POST["credentials"]
    user.save()
    return JsonResponse({"ok": True})


@csrf_exempt
@is_post
@personal_request
def get_all_tasks(request, user):
    tasks = serializers.serialize('json', Task.objects.filter(user=user, status="new"))
    return JsonResponse({"ok": True, "tasks": json.loads(tasks)})



@csrf_exempt
@is_post
@personal_request
def device_controll(request, user):
    token = request.POST["token"]
    return JsonResponse({"ok": True, "state": user.device_state})



@csrf_exempt
@is_post
@personal_request
def mark_done_task(request, user):
    task_order = int(request.POST["task_order"]) - 1
    tasks = Task.objects.filter(user=user, status="new")
    try:
        tasks[task_order].status = "done"
        tasks[task_order].save()
    except Exception as ex:
        print(kek)
    return JsonResponse({"ok": True})