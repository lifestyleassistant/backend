from django.db import models


class User(models.Model):
    user_id = models.CharField(max_length=64)
    name = models.CharField(max_length=64, default="NaN")
    age = models.IntegerField(default=-1)
    google_fit_credentials = models.TextField(default="None")
    device_state = models.CharField(default="off", max_length=16)


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=64, default="NaN")
    status = models.CharField(max_length=16, default="new")


class DailyHistory(models.Model):
    date = models.DateField()
    wake_up_time = models.TimeField()
    go_bed_time = models.TimeField()
    morning_mood = models.CharField(max_length=32)
    evening_mood = models.CharField(max_length=32)
    sleep_quantity = models.IntegerField()


class Sleep(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    wake_up_time = models.TimeField()
    go_bed_time = models.TimeField()
    wake_up_mode = models.IntegerField(default=-1)
    go_bed_mode = models.IntegerField(default=-1)

