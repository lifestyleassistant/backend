from django.contrib import admin
from .models import User, Task, DailyHistory, Sleep

admin.site.register(User)
admin.site.register(Task)
admin.site.register(DailyHistory)
admin.site.register(Sleep)
