from django.urls import path
from . import views


urlpatterns = [
    # User endpoints
    path('users/register/', views.register_user),
    path('users/setName/', views.set_user_name),
    path('users/setAge/', views.set_user_age),
    path('users/get/', views.get_user),
    path('users/setGoogleFitCode/', views.set_google_fit_code),

    # Task endpoints
    path('tasks/create/', views.create_task),
    path('tasks/all/', views.get_all_tasks),
    path('tasks/done/', views.mark_done_task),

    path('devices/state', views.device_controll),
]
